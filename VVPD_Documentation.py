"""
Группа КИ22-17/2Б Бекетов Александр
Вариант 4

Пример 1:
Введите необходимые слова латиницей через пробел. Qwerty lololo foo bar baz
->  Текущая сторона обрезки слова - слева.
    Текущая звук - гласный.
    Меню программы
    A - Выполнить задачу
    B - Поменять сторону обрезки слова
    C - Поменять звук (гласный или согласный)
    Введите команду или END:

Пример 2:
    Введите необходимые слова латиницей через пробел. qwer1 24 Ror13 f4
->  Неверный формат ввода слова, должны использоваться только буквы.
    Введите необходимые слова латиницей через пробел.
->  Вы ничего не ввели, попробуйте ещё раз.

Пример 3:
    Введите необходимые слова латиницей через пробел. Qwerty lololo foo bar baz
    Текущая сторона обрезки слова - слева.
    Введите команду или END: A
->  ['qwerty', 'l', 'foo', 'ba', 'b']
    Введите команду или END: B
->  Выбрана правая сторона обрезки слова.
    Введите команду или END: A
->  ['qwerty', 'o', 'foo', 'ar', 'z']


Пример 4:
    Введите необходимые слова латиницей через пробел. Qwerty lololo foo bar baz
    Текущая звук - гласный.
    Введите команду или END: A
->  ['qwerty', 'l', 'foo', 'ba', 'b']
    Введите команду или END: C
->  Выбран согласный звук.
    Введите команду или END: A
->  ['qwerty', 'lolol', 'foo', 'b', 'ba']
"""


def word_circumcision(words, side, sound):
    """
    Функция сокращения слова.

    Это функция сокращает слова в списке с левой или с правой стороны
    в зависимости от количества гласных или согласных звуков в предыдущем слове.
    Принимает на вход список слов и две строки.

    Args:
        words:  Список слов необходимых для сокращения
        side:   Сторона сокращения слова (left or right).
        sound:  Гласный или согласный звук, который будет
                подсчитываться в предыдущем слове (vowel or consonant).

    Returns:
        Новый список сокращённых слов.

    Examples:
        >> word_circumcision(['aaabbb', 'abcdefg', 'qwerty'], 'left', 'vowels')
        ['aaabbb', 'abc', 'qw']

        >> word_circumcision(['aaabbb', 'abcdefg', 'qwerty'], 'right', 'vowels')
        ['aaabbb', 'efg', 'ty']

        >> word_circumcision(['aaabbb', 'abcdefg', 'qwerty'], 'left', 'consonant')
        ['aaabbb', 'abc', 'qwert']
    """
    new_words = []
    for index, one_word in enumerate(words):
        if index == 0:
            new_words.append(one_word)
        if index + 1 < len(words):
            number = letter_count(one_word, sound)
            if side == 'left':
                if number == 0:
                    new_words.append(words[index + 1])
                else:
                    new_words.append(words[index + 1][:number])
            elif side == 'right':
                if number == 0:
                    new_words.append(words[index + 1])
                else:
                    new_words.append(words[index + 1][-number:])

    return new_words


def letter_count(word, sound):
    """
    Функция подсчёта гласных и согласных звуков.

    Эта функция перебирает буквы в слове и подсчитывает количество
    гласных и согласных. Принимает на вход слово и звук.

    Args:
        word:   Слово в котором будет считаться количетво
                гласных и согласных звуков.
        sound:  Гласный или согласный звук, колличество
                которого в слове нужно вернуть.

    Returns:
        Количество подсчитанного звука в слове.

    Examples:
        >> letter_count('balloon', vowels)
        3

        >> letter_count('balloon', consonants)
        4
    """
    vowel = 0
    consonant = 0
    all_vowels = ["a", "e", "i", "o", "u"]
    all_consonants = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm',
                      'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z']
    for letter in word:
        if letter in all_vowels:
            vowel += 1
        elif letter in all_consonants:
            consonant += 1
    if sound == 'vowels':
        return vowel
    elif sound == 'consonants':
        return consonant


def change_side(word_clipping_side):
    """
    Функция изменения стороны сокращения слова.

    Эта функуия принимает текущую сторону сокращения слова и
    возвращает противоположную сторону.

    Args:
        word_clipping_side: Предыдущая сторона сокращения слова.

    Returns:
        Новая сторона сокращения слова.

    Examples:
        >> change_side('left')
        'right'

        >> change_side('right')
        'left'
    """
    if word_clipping_side == 'left':
        print('Выбрана правая сторона обрезки слова.')
        return 'right'
    if word_clipping_side == 'right':
        print('Выбрана левая сторона обрезки слова.')
        return 'left'


def change_sound(sound):
    """
    Функция изменения звука (vowel or consonant) для подсчёта в слове.

    Эта функуия принимает текущий звук для подсчёта в слове и
    возвращает другой звук.

    Args:
        sound: Предыдущий звук.

    Returns:
        Новая звук.

    Examples:
        >> change_sound('vowels')
        'consonants'

        >> change_sound('consonants')
        'vowels'
    """
    if sound == 'vowels':
        print('Выбран согласный звук.')
        return 'consonants'
    if sound == 'consonants':
        print('Выбран гласный звук.')
        return 'vowels'


def main():
    while True:
        all_words = [i for i in input('Введите необходимые слова латиницей '
                                      'через пробел. ').lower().split()]
        if not len(all_words):
            print('Вы ничего не ввели, попробуйте ещё раз.')
            continue
        else:
            check = False
            for word in all_words:
                if not word.isalpha():
                    print('Неверный формат ввода слова, '
                          'должны использоваться только буквы.')
                    check = True
                    break
            if check:
                continue
            else:
                break

    clipping_side = 'left'
    letter_sound = 'vowels'
    print('Текущая сторона обрезки слова - слева.')
    print('Текущая звук - гласный.')

    menu = {
        'A': ('Выполнить задачу', word_circumcision),
        'B': ('Поменять сторону обрезки слова', change_side),
        'C': ('Поменять звук (гласный или согласный)', change_sound)
    }

    # Вывод меню на экран
    print('Меню программы')
    for k, v in menu.items():
        print(f'{k} - {v[0]}')

    # Ввод команды
    while True:
        command_work = [i for i in input('Введите команду или END: ').upper().split()]

        if not len(command_work):
            print('Вы ничего не ввели, попробуйте ещё раз.')

        if len(command_work) == 1:
            if command_work[0] == 'END':
                print('Вы завершили работу программы.')
                break
            elif command_work[0] == 'A':
                print(menu[command_work[0]][1](all_words, clipping_side, letter_sound))
            elif command_work[0] == 'B':
                clipping_side = menu[command_work[0]][1](clipping_side)
            elif command_work[0] == 'C':
                letter_sound = menu[command_work[0]][1](letter_sound)

            else:
                print('Такой команды не существует.')

        if len(command_work) > 1:
            print('Слишком много значений, попробуйте ещё раз.')


# Точка входа
if __name__ == '__main__':
    main()
